# Home Page - Top Slider

Note: themeimg shortcodes are replaced by the images in the media library.

Slider setting:

![settings](slider-settings.png)

Slider style:

![style](slider-style.png)

Item display: keep fields at 1

**Remember the short code, as it is needed for the home page.**

## Slide 1

![Slider 1](slider-1.png)

```html
<div class="slide-image">[themeimg src="home-slide-1.jpg"]</div>
<div class="slide-content">
<h3>VCN|LABS</h3>
<p>With over 30+ years of web design and development experience, the Vancouver Community Network offers website services for non-profits and large to small businesses at affordable rates.</p>
<p>The VCN|LABS web design and development services help support VCN's charitable efforts; providing free access to the Internet, email, and computer access to youth, seniors and vulnerable people in our community.</p>
<p><a href="https://vcnlabs.com">Visit the VCN|LABS site.</a></p>
<p>E-mail: webteam@vcn.bc.ca</p>
<p>Phone: 778-724-0622</p>
</div>
```

## Slide 2

![Slider 2](slider-2.png)

```html
<div class="slide-image">[themeimg src="home-slide-2.png"]</div>
<div class="slide-content">
<p>VCN has developed a web-based text messaging platform that will instantly connect street involved, disadvantaged and vulnerable people in Metro Vancouver with vital information about shelter, food services, health alerts and employment/training opportunities. </p>
<p>Starting in the Downtown Eastside, and using simple text messages, VCN will web-enable direct messaging to help end street homelessness, locate emergency shelters, send alerts about ‘bad batches’, missing persons, and job/skills training.</p>
<p><a href="//www2.vcn.bc.ca/get-involved/donate/">Donate Now!</a></p>
<p><a href="https://www.streetmessagingsystem.ca">Street Messaging System</a></p>
</div>
```

## Slide 3

![Slider 3](slider-3.png)

```html
<div class="slide-image">[themeimg src="home-slide-3.jpg"]</div>
<div class="slide-content">
<h3>Street Messaging System - Cineworks' Play It Forward Program</h3>
<p>In today’s information age, access to ICTs is essential. The Street Messaging Service is a unique and instant way of connecting community members to services operating in Downtown Eastside Vancouver. The SMS actively sends accessible and reliable information into the hands of those that need it most via text message.</p>
<p><a href="https://www.youtube.com/watch?v=LyTVRkdfqDo">Watch on YouTube</a></p>
<p><a href="//www2.vcn.bc.ca/get-involved/donate/">Donate Now!</a></p>
<p><a href="https://www.streetmessagingsystem.ca">Street Messaging System</a></p>
</div>
```

## Slide 4

![Slider 4](slider-4.png)

```html
<div class="slide-image">[themeimg src="home-slide-4.jpg"]</div>
<div class="slide-content">
<h3>Vancouver Community Network</h3>
<p>VCN is a non-profit Internet service provider that provides free services to assist individuals, community groups, and non-profit organizations in accessing and utilizing the Internet to its fullest ability.</p>
<p>We believe the information, resources and opportunities on the Internet should be accessible to all!</p>
</div>
```

## Slide 5

![Slider 5](slider-5.png)

```html
<div class="slide-image">[themeimg src="home-slide-5.jpg"]</div>
<div class="slide-content">
<h3>Vision Statement & Mission Statement</h3>
<p>The Vancouver Community Network strives to be an inclusive, multicultural, community-based organization which ensures the free, accessible electronic creation and exchange of the broadest range of information, experience, ideas and wisdom.</p>
<p>The Vancouver Community Network owns, operates and promotes a free, publicly accessible, non-commercial, community computer utility in the Lower Mainland of BC which provides a public space on the Internet.</p>
</div>
```
