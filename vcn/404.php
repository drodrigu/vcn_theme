<?php

// Remove default loop.
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'genesis_404' );
/**
 * This function outputs a 404 "Not Found" error message.
 *
 * @since 1.6
 */
function genesis_404() {
	?>
<h1 class="entry-title">404 - File Not found</h1>
<div class="entry-content">
	
<p>
	The page you are looking for no longer exists. Perhaps you can return back to the site's <a href="/">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.
</p>
<?php
	get_search_form();
	?>
	
</div>
<?php
}

genesis();
